# Compose Library Guidelines

go/androidx-compose-api-guidelines

<!--*
# Document freshness: For more information, see go/fresh-source.
freshness: { owner: 'seanmcq' reviewed: '2024-09-27' }
*-->

[TOC]

This guide is intended to guide developers in best practices for API design in
Jetpack Compose libraries.

Advice is primarily for developers of `androidx.compose` libraries. Other
library authors and app developers may find tips useful as well.

When appropriate, the distinction will be made between general Compose style
guidance and library-author specific guidance.

These guidelines are an extension of the
[AndroidX API Guidelines](/docs/api_guidelines/index.md) with
a target of Compose Framework Developers.

<!--#include file="/company/teams/androidx/api_guidelines/compose_api_guidelines/terms.md"-->

<!--#include file="/company/teams/androidx/api_guidelines/compose_api_guidelines/layering.md"-->

<!--#include file="/company/teams/androidx/api_guidelines/compose_api_guidelines/basic_patterns.md"-->

<!--#include file="/company/teams/androidx/api_guidelines/compose_api_guidelines/naming.md"-->
