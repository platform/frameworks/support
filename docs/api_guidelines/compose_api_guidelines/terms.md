
## Vocabulary {#vocabulary}

| Composable.       | @Composable component  | component               |
| ----------------- | ---------------------- | ----------------------- |
| Any `@Composable` | `@Composable` function | Short for `@Composable` |
: function          : that returns Unit and  : component               :
:                   : emits ui               :                         :

| Framework Author            | Developer                    | User          |
| --------------------------- | ---------------------------- | ------------- |
| Library author that creates | Developer using components   | User of a app |
: components or other         : from a library to build apps :               :
: composable APIs for use by  : for users.                   :               :
: developers                  :                              :               :

Framework advice primarily targets authors of `androidx.core` libraries, but is
intended to be generally applicable to other library authors.
