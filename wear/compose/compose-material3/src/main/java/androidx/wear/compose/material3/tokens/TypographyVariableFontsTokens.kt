/*
 * Copyright 2024 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// VERSION: v0_108
// GENERATED CODE - DO NOT MODIFY BY HAND
package androidx.wear.compose.material3.tokens

import androidx.annotation.RestrictTo
import androidx.compose.ui.text.font.FontVariation

/**
 * ********************************************************
 * Modified by hand, don't override!!!
 * *********************************************************
 */
@RestrictTo(RestrictTo.Scope.LIBRARY_GROUP)
public object TypographyVariableFontsTokens {
    public val ArcLargeVariationSettings: FontVariation.Settings =
        FontVariation.Settings(
            FontVariation.Setting("wdth", TypeScaleTokens.ArcLargeWidth),
            FontVariation.Setting("wght", TypeScaleTokens.ArcLargeWeight),
        )
    public val ArcMediumVariationSettings: FontVariation.Settings =
        FontVariation.Settings(
            FontVariation.Setting("wght", TypeScaleTokens.ArcMediumWeight),
            FontVariation.Setting("wdth", TypeScaleTokens.ArcMediumWidth),
        )
    public val ArcSmallVariationSettings: FontVariation.Settings =
        FontVariation.Settings(
            FontVariation.Setting("wght", TypeScaleTokens.ArcSmallWeight),
            FontVariation.Setting("wdth", TypeScaleTokens.ArcSmallWidth),
        )
    public val BodyExtraSmallVariationSettings: FontVariation.Settings =
        FontVariation.Settings(
            FontVariation.Setting("wght", TypeScaleTokens.BodyExtraSmallWeight),
            FontVariation.Setting("wdth", TypeScaleTokens.BodyExtraSmallWidth),
        )
    public val BodyLargeVariationSettings: FontVariation.Settings =
        FontVariation.Settings(
            FontVariation.Setting("wdth", TypeScaleTokens.BodyLargeWidth),
            FontVariation.Setting("wght", TypeScaleTokens.BodyLargeWeight),
        )
    public val BodyMediumVariationSettings: FontVariation.Settings =
        FontVariation.Settings(
            FontVariation.Setting("wght", TypeScaleTokens.BodyMediumWeight),
            FontVariation.Setting("wdth", TypeScaleTokens.BodyMediumWidth),
        )
    public val BodySmallVariationSettings: FontVariation.Settings =
        FontVariation.Settings(
            FontVariation.Setting("wght", TypeScaleTokens.BodySmallWeight),
            FontVariation.Setting("wdth", TypeScaleTokens.BodySmallWidth),
        )
    public val DisplayLargeVariationSettings: FontVariation.Settings =
        FontVariation.Settings(
            FontVariation.Setting("wght", TypeScaleTokens.DisplayLargeWeight),
            FontVariation.Setting("wdth", TypeScaleTokens.DisplayLargeWidth),
        )
    public val DisplayMediumVariationSettings: FontVariation.Settings =
        FontVariation.Settings(
            FontVariation.Setting("wdth", TypeScaleTokens.DisplayMediumWidth),
            FontVariation.Setting("wght", TypeScaleTokens.DisplayMediumWeight),
        )
    public val DisplaySmallVariationSettings: FontVariation.Settings =
        FontVariation.Settings(
            FontVariation.Setting("wdth", TypeScaleTokens.DisplaySmallWidth),
            FontVariation.Setting("wght", TypeScaleTokens.DisplaySmallWeight),
        )
    public val LabelLargeVariationSettings: FontVariation.Settings =
        FontVariation.Settings(
            FontVariation.Setting("wght", TypeScaleTokens.LabelLargeWeight),
            FontVariation.Setting("wdth", TypeScaleTokens.LabelLargeWidth),
        )
    public val LabelMediumVariationSettings: FontVariation.Settings =
        FontVariation.Settings(
            FontVariation.Setting("wdth", TypeScaleTokens.LabelMediumWidth),
            FontVariation.Setting("wght", TypeScaleTokens.LabelMediumWeight),
        )
    public val LabelSmallVariationSettings: FontVariation.Settings =
        FontVariation.Settings(
            FontVariation.Setting("wght", TypeScaleTokens.LabelSmallWeight),
            FontVariation.Setting("wdth", TypeScaleTokens.LabelSmallWidth),
        )
    public val NumeralExtraLargeVariationSettings: FontVariation.Settings =
        FontVariation.Settings(
            FontVariation.Setting("wdth", TypeScaleTokens.NumeralExtraLargeWidth),
            FontVariation.Setting("wght", TypeScaleTokens.NumeralExtraLargeWeight),
        )
    public val NumeralExtraSmallVariationSettings: FontVariation.Settings =
        FontVariation.Settings(
            FontVariation.Setting("wght", TypeScaleTokens.NumeralExtraSmallWeight),
            FontVariation.Setting("wdth", TypeScaleTokens.NumeralExtraSmallWidth),
        )
    public val NumeralLargeVariationSettings: FontVariation.Settings =
        FontVariation.Settings(
            FontVariation.Setting("wght", TypeScaleTokens.NumeralLargeWeight),
            FontVariation.Setting("wdth", TypeScaleTokens.NumeralLargeWidth),
        )
    public val NumeralMediumVariationSettings: FontVariation.Settings =
        FontVariation.Settings(
            FontVariation.Setting("wght", TypeScaleTokens.NumeralMediumWeight),
            FontVariation.Setting("wdth", TypeScaleTokens.NumeralMediumWidth),
        )
    public val NumeralSmallVariationSettings: FontVariation.Settings =
        FontVariation.Settings(
            FontVariation.Setting("wdth", TypeScaleTokens.NumeralSmallWidth),
            FontVariation.Setting("wght", TypeScaleTokens.NumeralSmallWeight),
        )
    public val TitleLargeVariationSettings: FontVariation.Settings =
        FontVariation.Settings(
            FontVariation.Setting("wdth", TypeScaleTokens.TitleLargeWidth),
            FontVariation.Setting("wght", TypeScaleTokens.TitleLargeWeight),
        )
    public val TitleMediumVariationSettings: FontVariation.Settings =
        FontVariation.Settings(
            FontVariation.Setting("wdth", TypeScaleTokens.TitleMediumWidth),
            FontVariation.Setting("wght", TypeScaleTokens.TitleMediumWeight),
        )
    public val TitleSmallVariationSettings: FontVariation.Settings =
        FontVariation.Settings(
            FontVariation.Setting("wght", TypeScaleTokens.TitleSmallWeight),
            FontVariation.Setting("wdth", TypeScaleTokens.TitleSmallWidth),
        )
}
