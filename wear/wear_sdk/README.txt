name: "wear-sdk-stubs"
description:
    "Stub jar for Wear SDK public API available for use in 3P apps."
    "The implementation associated with this version containing are"
    "preinstalled on WearOS devices."
gerrit source: "vendor/google_clockwork/sdk/lib"
API version: 36.0
Build ID: 13021874
Last updated: Thu Feb  6 02:51:00 PM UTC 2025
