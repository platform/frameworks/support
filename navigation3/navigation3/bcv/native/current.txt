// Klib ABI Dump
// Targets: [linuxX64.linuxx64Stubs]
// Rendering settings:
// - Signature version: 2
// - Show manifest properties: true
// - Show declarations: true

// Library unique name: <androidx.navigation3:navigation3>
open annotation class androidx.navigation3/EntryDsl : kotlin/Annotation { // androidx.navigation3/EntryDsl|null[0]
    constructor <init>() // androidx.navigation3/EntryDsl.<init>|<init>(){}[0]
}

abstract interface androidx.navigation3/NavLocalProvider { // androidx.navigation3/NavLocalProvider|null[0]
    abstract fun <#A1: kotlin/Any> ProvideToEntry(androidx.navigation3/NavEntry<#A1>) // androidx.navigation3/NavLocalProvider.ProvideToEntry|ProvideToEntry(androidx.navigation3.NavEntry<0:0>){0§<kotlin.Any>}[0]
    open fun ProvideToBackStack(kotlin.collections/List<kotlin/Any>, kotlin/Function0<kotlin/Unit>) // androidx.navigation3/NavLocalProvider.ProvideToBackStack|ProvideToBackStack(kotlin.collections.List<kotlin.Any>;kotlin.Function0<kotlin.Unit>){}[0]
}

final class <#A: kotlin/Any> androidx.navigation3/EntryClassProvider { // androidx.navigation3/EntryClassProvider|null[0]
    constructor <init>(kotlin.reflect/KClass<#A>, kotlin.collections/Map<kotlin/String, kotlin/Any>, kotlin/Function1<#A, kotlin/Unit>) // androidx.navigation3/EntryClassProvider.<init>|<init>(kotlin.reflect.KClass<1:0>;kotlin.collections.Map<kotlin.String,kotlin.Any>;kotlin.Function1<1:0,kotlin.Unit>){}[0]

    final val clazz // androidx.navigation3/EntryClassProvider.clazz|{}clazz[0]
        final fun <get-clazz>(): kotlin.reflect/KClass<#A> // androidx.navigation3/EntryClassProvider.clazz.<get-clazz>|<get-clazz>(){}[0]
    final val content // androidx.navigation3/EntryClassProvider.content|{}content[0]
        final fun <get-content>(): kotlin/Function1<#A, kotlin/Unit> // androidx.navigation3/EntryClassProvider.content.<get-content>|<get-content>(){}[0]
    final val featureMap // androidx.navigation3/EntryClassProvider.featureMap|{}featureMap[0]
        final fun <get-featureMap>(): kotlin.collections/Map<kotlin/String, kotlin/Any> // androidx.navigation3/EntryClassProvider.featureMap.<get-featureMap>|<get-featureMap>(){}[0]

    final fun component1(): kotlin.reflect/KClass<#A> // androidx.navigation3/EntryClassProvider.component1|component1(){}[0]
    final fun component2(): kotlin.collections/Map<kotlin/String, kotlin/Any> // androidx.navigation3/EntryClassProvider.component2|component2(){}[0]
    final fun component3(): kotlin/Function1<#A, kotlin/Unit> // androidx.navigation3/EntryClassProvider.component3|component3(){}[0]
    final fun copy(kotlin.reflect/KClass<#A> = ..., kotlin.collections/Map<kotlin/String, kotlin/Any> = ..., kotlin/Function1<#A, kotlin/Unit> = ...): androidx.navigation3/EntryClassProvider<#A> // androidx.navigation3/EntryClassProvider.copy|copy(kotlin.reflect.KClass<1:0>;kotlin.collections.Map<kotlin.String,kotlin.Any>;kotlin.Function1<1:0,kotlin.Unit>){}[0]
    final fun equals(kotlin/Any?): kotlin/Boolean // androidx.navigation3/EntryClassProvider.equals|equals(kotlin.Any?){}[0]
    final fun hashCode(): kotlin/Int // androidx.navigation3/EntryClassProvider.hashCode|hashCode(){}[0]
    final fun toString(): kotlin/String // androidx.navigation3/EntryClassProvider.toString|toString(){}[0]
}

final class <#A: kotlin/Any> androidx.navigation3/EntryProvider { // androidx.navigation3/EntryProvider|null[0]
    constructor <init>(#A, kotlin.collections/Map<kotlin/String, kotlin/Any>, kotlin/Function1<#A, kotlin/Unit>) // androidx.navigation3/EntryProvider.<init>|<init>(1:0;kotlin.collections.Map<kotlin.String,kotlin.Any>;kotlin.Function1<1:0,kotlin.Unit>){}[0]

    final val content // androidx.navigation3/EntryProvider.content|{}content[0]
        final fun <get-content>(): kotlin/Function1<#A, kotlin/Unit> // androidx.navigation3/EntryProvider.content.<get-content>|<get-content>(){}[0]
    final val featureMap // androidx.navigation3/EntryProvider.featureMap|{}featureMap[0]
        final fun <get-featureMap>(): kotlin.collections/Map<kotlin/String, kotlin/Any> // androidx.navigation3/EntryProvider.featureMap.<get-featureMap>|<get-featureMap>(){}[0]
    final val key // androidx.navigation3/EntryProvider.key|{}key[0]
        final fun <get-key>(): #A // androidx.navigation3/EntryProvider.key.<get-key>|<get-key>(){}[0]

    final fun component1(): #A // androidx.navigation3/EntryProvider.component1|component1(){}[0]
    final fun component2(): kotlin.collections/Map<kotlin/String, kotlin/Any> // androidx.navigation3/EntryProvider.component2|component2(){}[0]
    final fun component3(): kotlin/Function1<#A, kotlin/Unit> // androidx.navigation3/EntryProvider.component3|component3(){}[0]
    final fun copy(#A = ..., kotlin.collections/Map<kotlin/String, kotlin/Any> = ..., kotlin/Function1<#A, kotlin/Unit> = ...): androidx.navigation3/EntryProvider<#A> // androidx.navigation3/EntryProvider.copy|copy(1:0;kotlin.collections.Map<kotlin.String,kotlin.Any>;kotlin.Function1<1:0,kotlin.Unit>){}[0]
    final fun equals(kotlin/Any?): kotlin/Boolean // androidx.navigation3/EntryProvider.equals|equals(kotlin.Any?){}[0]
    final fun hashCode(): kotlin/Int // androidx.navigation3/EntryProvider.hashCode|hashCode(){}[0]
    final fun toString(): kotlin/String // androidx.navigation3/EntryProvider.toString|toString(){}[0]
}

final class <#A: kotlin/Any> androidx.navigation3/EntryProviderBuilder { // androidx.navigation3/EntryProviderBuilder|null[0]
    constructor <init>(kotlin/Function1<#A, androidx.navigation3/NavEntry<#A>>) // androidx.navigation3/EntryProviderBuilder.<init>|<init>(kotlin.Function1<1:0,androidx.navigation3.NavEntry<1:0>>){}[0]

    final fun <#A1: kotlin/Any> addEntryProvider(#A1, kotlin.collections/Map<kotlin/String, kotlin/Any> = ..., kotlin/Function1<#A1, kotlin/Unit>) // androidx.navigation3/EntryProviderBuilder.addEntryProvider|addEntryProvider(0:0;kotlin.collections.Map<kotlin.String,kotlin.Any>;kotlin.Function1<0:0,kotlin.Unit>){0§<kotlin.Any>}[0]
    final fun <#A1: kotlin/Any> addEntryProvider(kotlin.reflect/KClass<#A1>, kotlin.collections/Map<kotlin/String, kotlin/Any> = ..., kotlin/Function1<#A1, kotlin/Unit>) // androidx.navigation3/EntryProviderBuilder.addEntryProvider|addEntryProvider(kotlin.reflect.KClass<0:0>;kotlin.collections.Map<kotlin.String,kotlin.Any>;kotlin.Function1<0:0,kotlin.Unit>){0§<kotlin.Any>}[0]
    final fun build(): kotlin/Function1<#A, androidx.navigation3/NavEntry<#A>> // androidx.navigation3/EntryProviderBuilder.build|build(){}[0]
}

open class <#A: kotlin/Any> androidx.navigation3/NavEntry { // androidx.navigation3/NavEntry|null[0]
    constructor <init>(#A, kotlin.collections/Map<kotlin/String, kotlin/Any> = ..., kotlin/Function1<#A, kotlin/Unit>) // androidx.navigation3/NavEntry.<init>|<init>(1:0;kotlin.collections.Map<kotlin.String,kotlin.Any>;kotlin.Function1<1:0,kotlin.Unit>){}[0]

    open val content // androidx.navigation3/NavEntry.content|{}content[0]
        open fun <get-content>(): kotlin/Function1<#A, kotlin/Unit> // androidx.navigation3/NavEntry.content.<get-content>|<get-content>(){}[0]
    open val featureMap // androidx.navigation3/NavEntry.featureMap|{}featureMap[0]
        open fun <get-featureMap>(): kotlin.collections/Map<kotlin/String, kotlin/Any> // androidx.navigation3/NavEntry.featureMap.<get-featureMap>|<get-featureMap>(){}[0]
    open val key // androidx.navigation3/NavEntry.key|{}key[0]
        open fun <get-key>(): #A // androidx.navigation3/NavEntry.key.<get-key>|<get-key>(){}[0]
}

open class <#A: kotlin/Any> androidx.navigation3/NavEntryWrapper : androidx.navigation3/NavEntry<#A> { // androidx.navigation3/NavEntryWrapper|null[0]
    constructor <init>(androidx.navigation3/NavEntry<#A>) // androidx.navigation3/NavEntryWrapper.<init>|<init>(androidx.navigation3.NavEntry<1:0>){}[0]

    final val navEntry // androidx.navigation3/NavEntryWrapper.navEntry|{}navEntry[0]
        final fun <get-navEntry>(): androidx.navigation3/NavEntry<#A> // androidx.navigation3/NavEntryWrapper.navEntry.<get-navEntry>|<get-navEntry>(){}[0]
    open val content // androidx.navigation3/NavEntryWrapper.content|{}content[0]
        open fun <get-content>(): kotlin/Function1<#A, kotlin/Unit> // androidx.navigation3/NavEntryWrapper.content.<get-content>|<get-content>(){}[0]
    open val featureMap // androidx.navigation3/NavEntryWrapper.featureMap|{}featureMap[0]
        open fun <get-featureMap>(): kotlin.collections/Map<kotlin/String, kotlin/Any> // androidx.navigation3/NavEntryWrapper.featureMap.<get-featureMap>|<get-featureMap>(){}[0]
    open val key // androidx.navigation3/NavEntryWrapper.key|{}key[0]
        open fun <get-key>(): #A // androidx.navigation3/NavEntryWrapper.key.<get-key>|<get-key>(){}[0]
}

final object androidx.navigation3/SaveableStateNavLocalProvider : androidx.navigation3/NavLocalProvider { // androidx.navigation3/SaveableStateNavLocalProvider|null[0]
    final fun <#A1: kotlin/Any> ProvideToEntry(androidx.navigation3/NavEntry<#A1>) // androidx.navigation3/SaveableStateNavLocalProvider.ProvideToEntry|ProvideToEntry(androidx.navigation3.NavEntry<0:0>){0§<kotlin.Any>}[0]
    final fun ProvideToBackStack(kotlin.collections/List<kotlin/Any>, kotlin/Function0<kotlin/Unit>) // androidx.navigation3/SaveableStateNavLocalProvider.ProvideToBackStack|ProvideToBackStack(kotlin.collections.List<kotlin.Any>;kotlin.Function0<kotlin.Unit>){}[0]
}

final object androidx.navigation3/SavedStateNavLocalProvider : androidx.navigation3/NavLocalProvider { // androidx.navigation3/SavedStateNavLocalProvider|null[0]
    final fun <#A1: kotlin/Any> ProvideToEntry(androidx.navigation3/NavEntry<#A1>) // androidx.navigation3/SavedStateNavLocalProvider.ProvideToEntry|ProvideToEntry(androidx.navigation3.NavEntry<0:0>){0§<kotlin.Any>}[0]
}

final fun <#A: kotlin/Any> (androidx.navigation3/EntryProviderBuilder<#A>).androidx.navigation3/entry(#A, kotlin.collections/Map<kotlin/String, kotlin/Any> = ..., kotlin/Function1<#A, kotlin/Unit>) // androidx.navigation3/entry|entry@androidx.navigation3.EntryProviderBuilder<0:0>(0:0;kotlin.collections.Map<kotlin.String,kotlin.Any>;kotlin.Function1<0:0,kotlin.Unit>){0§<kotlin.Any>}[0]
final fun <#A: kotlin/Any> androidx.navigation3/NavBackStackProvider(kotlin.collections/List<#A>, kotlin/Function1<#A, androidx.navigation3/NavEntry<out #A>>, kotlin.collections/List<androidx.navigation3/NavLocalProvider> = ..., kotlin/Function1<kotlin.collections/List<androidx.navigation3/NavEntry<#A>>, kotlin/Unit>) // androidx.navigation3/NavBackStackProvider|NavBackStackProvider(kotlin.collections.List<0:0>;kotlin.Function1<0:0,androidx.navigation3.NavEntry<out|0:0>>;kotlin.collections.List<androidx.navigation3.NavLocalProvider>;kotlin.Function1<kotlin.collections.List<androidx.navigation3.NavEntry<0:0>>,kotlin.Unit>){0§<kotlin.Any>}[0]
final inline fun <#A: kotlin/Any> androidx.navigation3/entryProvider(noinline kotlin/Function1<#A, androidx.navigation3/NavEntry<#A>> = ..., kotlin/Function1<androidx.navigation3/EntryProviderBuilder<#A>, kotlin/Unit>): kotlin/Function1<#A, androidx.navigation3/NavEntry<#A>> // androidx.navigation3/entryProvider|entryProvider(kotlin.Function1<0:0,androidx.navigation3.NavEntry<0:0>>;kotlin.Function1<androidx.navigation3.EntryProviderBuilder<0:0>,kotlin.Unit>){0§<kotlin.Any>}[0]
final inline fun <#A: reified kotlin/Any> (androidx.navigation3/EntryProviderBuilder<*>).androidx.navigation3/entry(kotlin.collections/Map<kotlin/String, kotlin/Any> = ..., noinline kotlin/Function1<#A, kotlin/Unit>) // androidx.navigation3/entry|entry@androidx.navigation3.EntryProviderBuilder<*>(kotlin.collections.Map<kotlin.String,kotlin.Any>;kotlin.Function1<0:0,kotlin.Unit>){0§<kotlin.Any>}[0]
