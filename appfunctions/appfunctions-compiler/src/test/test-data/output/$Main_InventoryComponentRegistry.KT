package appfunctions_aggregated_deps

import androidx.appfunctions.AppFunctionComponentRegistry
import javax.`annotation`.processing.Generated

@AppFunctionComponentRegistry(
  componentCategory = "INVENTORY",
  componentNames = [
    "com.testdata.${'$'}AllPrimitiveInputFunctions_AppFunctionInventory",
    "com.testdata.${'$'}SimpleFunction_AppFunctionInventory",
    "com.testdata.diff.${'$'}SimpleFunction_AppFunctionInventory",
  ],
)
@Generated("androidx.appfunctions.compiler.AppFunctionCompiler")
public class `$Main_InventoryComponentRegistry`
