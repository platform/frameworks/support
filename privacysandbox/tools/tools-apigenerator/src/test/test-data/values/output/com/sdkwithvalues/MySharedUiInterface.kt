package com.sdkwithvalues

import androidx.privacysandbox.ui.core.SharedUiAdapter

public interface MySharedUiInterface : SharedUiAdapter {
    public fun doUiStuff()
}
