package com.sdk

import androidx.privacysandbox.ui.core.SharedUiAdapter

public interface MySharedUiInterface : SharedUiAdapter {
    public fun doStuff()
}
