/*
 * Copyright 2024 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package androidx.xr.runtime.internal

import androidx.annotation.RestrictTo

/** Interface for a XR Runtime [GltfEntity]. */
@RestrictTo(RestrictTo.Scope.LIBRARY_GROUP_PREFIX)
public interface GltfEntity : Entity {

    /**
     * Starts the animation with the given name.
     *
     * @param animationName The name of the animation to start. If null is supplied, will play the
     *   first animation found in the glTF.
     * @param loop Whether the animation should loop.
     */
    public fun startAnimation(loop: Boolean, animationName: String?)

    /** Stops the animation of the glTF entity. */
    public fun stopAnimation()

    /** Returns the current animation state of the glTF entity. */
    @AnimationState public val animationState: Int

    // TODO: b/362368652 - Add an OnAnimationFinished() Listener interface
    //                     Add a getAnimationTimeRemaining() interface

    /** Specifies the current animation state of the [GltfEntity]. */
    @RestrictTo(RestrictTo.Scope.LIBRARY)
    public annotation class AnimationState {
        public companion object {
            public const val PLAYING: Int = 0
            public const val STOPPED: Int = 1
        }
    }
}
