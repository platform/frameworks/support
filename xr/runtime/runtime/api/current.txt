// Signature format: 4.0
package androidx.xr.runtime.math {

  public final class MathHelper {
    method public static float clamp(float x, float min, float max);
    method public static float lerp(float a, float b, float t);
    method public static float toDegrees(float angleInRadians);
    method public static float toRadians(float angleInDegrees);
  }

  public final class Matrix4 {
    ctor public Matrix4(androidx.xr.runtime.math.Matrix4 other);
    ctor public Matrix4(float[] dataToCopy);
    method public androidx.xr.runtime.math.Matrix4 copy(optional float[] data);
    method public static androidx.xr.runtime.math.Matrix4 fromPose(androidx.xr.runtime.math.Pose pose);
    method public static androidx.xr.runtime.math.Matrix4 fromQuaternion(androidx.xr.runtime.math.Quaternion quaternion);
    method public static androidx.xr.runtime.math.Matrix4 fromScale(androidx.xr.runtime.math.Vector3 scale);
    method public static androidx.xr.runtime.math.Matrix4 fromScale(float scale);
    method public static androidx.xr.runtime.math.Matrix4 fromTranslation(androidx.xr.runtime.math.Vector3 translation);
    method public static androidx.xr.runtime.math.Matrix4 fromTrs(androidx.xr.runtime.math.Vector3 translation, androidx.xr.runtime.math.Quaternion rotation, androidx.xr.runtime.math.Vector3 scale);
    method public float[] getData();
    method public androidx.xr.runtime.math.Matrix4 getInverse();
    method public androidx.xr.runtime.math.Pose getPose();
    method public androidx.xr.runtime.math.Quaternion getRotation();
    method public androidx.xr.runtime.math.Vector3 getScale();
    method public androidx.xr.runtime.math.Vector3 getTranslation();
    method public androidx.xr.runtime.math.Matrix4 getTranspose();
    method public boolean isTrs();
    method public operator androidx.xr.runtime.math.Matrix4 times(androidx.xr.runtime.math.Matrix4 other);
    property public float[] data;
    property public androidx.xr.runtime.math.Matrix4 inverse;
    property public boolean isTrs;
    property public androidx.xr.runtime.math.Pose pose;
    property public androidx.xr.runtime.math.Quaternion rotation;
    property public androidx.xr.runtime.math.Vector3 scale;
    property public androidx.xr.runtime.math.Vector3 translation;
    property public androidx.xr.runtime.math.Matrix4 transpose;
    field public static final androidx.xr.runtime.math.Matrix4.Companion Companion;
    field public static final androidx.xr.runtime.math.Matrix4 Identity;
    field public static final androidx.xr.runtime.math.Matrix4 Zero;
  }

  public static final class Matrix4.Companion {
    method public androidx.xr.runtime.math.Matrix4 fromPose(androidx.xr.runtime.math.Pose pose);
    method public androidx.xr.runtime.math.Matrix4 fromQuaternion(androidx.xr.runtime.math.Quaternion quaternion);
    method public androidx.xr.runtime.math.Matrix4 fromScale(androidx.xr.runtime.math.Vector3 scale);
    method public androidx.xr.runtime.math.Matrix4 fromScale(float scale);
    method public androidx.xr.runtime.math.Matrix4 fromTranslation(androidx.xr.runtime.math.Vector3 translation);
    method public androidx.xr.runtime.math.Matrix4 fromTrs(androidx.xr.runtime.math.Vector3 translation, androidx.xr.runtime.math.Quaternion rotation, androidx.xr.runtime.math.Vector3 scale);
    property public androidx.xr.runtime.math.Matrix4 Identity;
    property public androidx.xr.runtime.math.Matrix4 Zero;
  }

  public final class Pose {
    ctor public Pose();
    ctor public Pose(androidx.xr.runtime.math.Pose other);
    ctor public Pose(optional androidx.xr.runtime.math.Vector3 translation);
    ctor public Pose(optional androidx.xr.runtime.math.Vector3 translation, optional androidx.xr.runtime.math.Quaternion rotation);
    method public infix androidx.xr.runtime.math.Pose compose(androidx.xr.runtime.math.Pose other);
    method public androidx.xr.runtime.math.Pose copy();
    method public androidx.xr.runtime.math.Pose copy(optional androidx.xr.runtime.math.Vector3 translation);
    method public androidx.xr.runtime.math.Pose copy(optional androidx.xr.runtime.math.Vector3 translation, optional androidx.xr.runtime.math.Quaternion rotation);
    method public static float distance(androidx.xr.runtime.math.Pose lhs, androidx.xr.runtime.math.Pose rhs);
    method public static androidx.xr.runtime.math.Pose fromLookAt(androidx.xr.runtime.math.Vector3 eye, androidx.xr.runtime.math.Vector3 target);
    method public static androidx.xr.runtime.math.Pose fromLookAt(androidx.xr.runtime.math.Vector3 eye, androidx.xr.runtime.math.Vector3 target, optional androidx.xr.runtime.math.Vector3 up);
    method public inline androidx.xr.runtime.math.Vector3 getBackward();
    method public inline androidx.xr.runtime.math.Vector3 getDown();
    method public inline androidx.xr.runtime.math.Vector3 getForward();
    method public androidx.xr.runtime.math.Pose getInverse();
    method public inline androidx.xr.runtime.math.Vector3 getLeft();
    method public inline androidx.xr.runtime.math.Vector3 getRight();
    method public androidx.xr.runtime.math.Quaternion getRotation();
    method public androidx.xr.runtime.math.Vector3 getTranslation();
    method public inline androidx.xr.runtime.math.Vector3 getUp();
    method public static androidx.xr.runtime.math.Pose lerp(androidx.xr.runtime.math.Pose start, androidx.xr.runtime.math.Pose end, float ratio);
    method public androidx.xr.runtime.math.Pose rotate(androidx.xr.runtime.math.Quaternion rotation);
    method public infix androidx.xr.runtime.math.Vector3 transformPoint(androidx.xr.runtime.math.Vector3 point);
    method public infix androidx.xr.runtime.math.Vector3 transformVector(androidx.xr.runtime.math.Vector3 vector);
    method public androidx.xr.runtime.math.Pose translate(androidx.xr.runtime.math.Vector3 translation);
    property public inline androidx.xr.runtime.math.Vector3 backward;
    property public inline androidx.xr.runtime.math.Vector3 down;
    property public inline androidx.xr.runtime.math.Vector3 forward;
    property public androidx.xr.runtime.math.Pose inverse;
    property public inline androidx.xr.runtime.math.Vector3 left;
    property public inline androidx.xr.runtime.math.Vector3 right;
    property public androidx.xr.runtime.math.Quaternion rotation;
    property public androidx.xr.runtime.math.Vector3 translation;
    property public inline androidx.xr.runtime.math.Vector3 up;
    field public static final androidx.xr.runtime.math.Pose.Companion Companion;
    field public static final androidx.xr.runtime.math.Pose Identity;
  }

  public static final class Pose.Companion {
    method public float distance(androidx.xr.runtime.math.Pose lhs, androidx.xr.runtime.math.Pose rhs);
    method public androidx.xr.runtime.math.Pose fromLookAt(androidx.xr.runtime.math.Vector3 eye, androidx.xr.runtime.math.Vector3 target);
    method public androidx.xr.runtime.math.Pose fromLookAt(androidx.xr.runtime.math.Vector3 eye, androidx.xr.runtime.math.Vector3 target, optional androidx.xr.runtime.math.Vector3 up);
    method public androidx.xr.runtime.math.Pose lerp(androidx.xr.runtime.math.Pose start, androidx.xr.runtime.math.Pose end, float ratio);
    property public androidx.xr.runtime.math.Pose Identity;
  }

  public final class Quaternion {
    ctor public Quaternion();
    ctor public Quaternion(androidx.xr.runtime.math.Quaternion other);
    ctor public Quaternion(optional float x);
    ctor public Quaternion(optional float x, optional float y);
    ctor public Quaternion(optional float x, optional float y, optional float z);
    ctor public Quaternion(optional float x, optional float y, optional float z, optional float w);
    method public static float angle(androidx.xr.runtime.math.Quaternion start, androidx.xr.runtime.math.Quaternion end);
    method public androidx.xr.runtime.math.Quaternion copy();
    method public androidx.xr.runtime.math.Quaternion copy(optional float x);
    method public androidx.xr.runtime.math.Quaternion copy(optional float x, optional float y);
    method public androidx.xr.runtime.math.Quaternion copy(optional float x, optional float y, optional float z);
    method public androidx.xr.runtime.math.Quaternion copy(optional float x, optional float y, optional float z, optional float w);
    method public operator androidx.xr.runtime.math.Quaternion div(float c);
    method public inline infix float dot(androidx.xr.runtime.math.Quaternion other);
    method public static float dot(androidx.xr.runtime.math.Quaternion lhs, androidx.xr.runtime.math.Quaternion rhs);
    method public static androidx.xr.runtime.math.Quaternion fromAxisAngle(androidx.xr.runtime.math.Vector3 axis, float degrees);
    method public static androidx.xr.runtime.math.Quaternion fromEulerAngles(androidx.xr.runtime.math.Vector3 eulerAngles);
    method public static androidx.xr.runtime.math.Quaternion fromEulerAngles(float pitch, float yaw, float roll);
    method public static androidx.xr.runtime.math.Quaternion fromLookTowards(androidx.xr.runtime.math.Vector3 forward, androidx.xr.runtime.math.Vector3 up);
    method public static androidx.xr.runtime.math.Quaternion fromRotation(androidx.xr.runtime.math.Quaternion start, androidx.xr.runtime.math.Quaternion end);
    method public static androidx.xr.runtime.math.Quaternion fromRotation(androidx.xr.runtime.math.Vector3 start, androidx.xr.runtime.math.Vector3 end);
    method public kotlin.Pair<androidx.xr.runtime.math.Vector3,java.lang.Float> getAxisAngle();
    method public androidx.xr.runtime.math.Vector3 getEulerAngles();
    method public inline androidx.xr.runtime.math.Quaternion getInverse();
    method public float getW();
    method public float getX();
    method public float getY();
    method public float getZ();
    method public static androidx.xr.runtime.math.Quaternion lerp(androidx.xr.runtime.math.Quaternion start, androidx.xr.runtime.math.Quaternion end, float ratio);
    method public inline operator androidx.xr.runtime.math.Quaternion minus(androidx.xr.runtime.math.Quaternion other);
    method public inline operator androidx.xr.runtime.math.Quaternion plus(androidx.xr.runtime.math.Quaternion other);
    method public static androidx.xr.runtime.math.Quaternion slerp(androidx.xr.runtime.math.Quaternion start, androidx.xr.runtime.math.Quaternion end, float ratio);
    method public inline operator androidx.xr.runtime.math.Quaternion times(androidx.xr.runtime.math.Quaternion other);
    method public inline operator androidx.xr.runtime.math.Vector3 times(androidx.xr.runtime.math.Vector3 src);
    method public operator androidx.xr.runtime.math.Quaternion times(float c);
    method public androidx.xr.runtime.math.Quaternion toNormalized();
    method public inline operator androidx.xr.runtime.math.Quaternion unaryMinus();
    property public kotlin.Pair<androidx.xr.runtime.math.Vector3,java.lang.Float> axisAngle;
    property public androidx.xr.runtime.math.Vector3 eulerAngles;
    property public inline androidx.xr.runtime.math.Quaternion inverse;
    property public float w;
    property public float x;
    property public float y;
    property public float z;
    field public static final androidx.xr.runtime.math.Quaternion.Companion Companion;
    field public static final androidx.xr.runtime.math.Quaternion Identity;
  }

  public static final class Quaternion.Companion {
    method public float angle(androidx.xr.runtime.math.Quaternion start, androidx.xr.runtime.math.Quaternion end);
    method public float dot(androidx.xr.runtime.math.Quaternion lhs, androidx.xr.runtime.math.Quaternion rhs);
    method public androidx.xr.runtime.math.Quaternion fromAxisAngle(androidx.xr.runtime.math.Vector3 axis, float degrees);
    method public androidx.xr.runtime.math.Quaternion fromEulerAngles(androidx.xr.runtime.math.Vector3 eulerAngles);
    method public androidx.xr.runtime.math.Quaternion fromEulerAngles(float pitch, float yaw, float roll);
    method public androidx.xr.runtime.math.Quaternion fromLookTowards(androidx.xr.runtime.math.Vector3 forward, androidx.xr.runtime.math.Vector3 up);
    method public androidx.xr.runtime.math.Quaternion fromRotation(androidx.xr.runtime.math.Quaternion start, androidx.xr.runtime.math.Quaternion end);
    method public androidx.xr.runtime.math.Quaternion fromRotation(androidx.xr.runtime.math.Vector3 start, androidx.xr.runtime.math.Vector3 end);
    method public androidx.xr.runtime.math.Quaternion lerp(androidx.xr.runtime.math.Quaternion start, androidx.xr.runtime.math.Quaternion end, float ratio);
    method public androidx.xr.runtime.math.Quaternion slerp(androidx.xr.runtime.math.Quaternion start, androidx.xr.runtime.math.Quaternion end, float ratio);
    property public androidx.xr.runtime.math.Quaternion Identity;
  }

  public final class Ray {
    ctor public Ray();
    ctor public Ray(androidx.xr.runtime.math.Ray other);
    ctor public Ray(optional androidx.xr.runtime.math.Vector3 origin, optional androidx.xr.runtime.math.Vector3 direction);
    method public androidx.xr.runtime.math.Vector3 getDirection();
    method public androidx.xr.runtime.math.Vector3 getOrigin();
    property public androidx.xr.runtime.math.Vector3 direction;
    property public androidx.xr.runtime.math.Vector3 origin;
  }

  public final class Vector2 {
    ctor public Vector2();
    ctor public Vector2(androidx.xr.runtime.math.Vector2 other);
    ctor public Vector2(optional float x);
    ctor public Vector2(optional float x, optional float y);
    method public static androidx.xr.runtime.math.Vector2 abs(androidx.xr.runtime.math.Vector2 vector);
    method public static float angularDistance(androidx.xr.runtime.math.Vector2 vector1, androidx.xr.runtime.math.Vector2 vector2);
    method public androidx.xr.runtime.math.Vector2 clamp(androidx.xr.runtime.math.Vector2 min, androidx.xr.runtime.math.Vector2 max);
    method public inline androidx.xr.runtime.math.Vector2 copy();
    method public inline androidx.xr.runtime.math.Vector2 copy(optional float x);
    method public inline androidx.xr.runtime.math.Vector2 copy(optional float x, optional float y);
    method public inline infix float cross(androidx.xr.runtime.math.Vector2 other);
    method public static float distance(androidx.xr.runtime.math.Vector2 vector1, androidx.xr.runtime.math.Vector2 vector2);
    method public inline operator androidx.xr.runtime.math.Vector2 div(androidx.xr.runtime.math.Vector2 other);
    method public inline operator androidx.xr.runtime.math.Vector2 div(float c);
    method public inline infix float dot(androidx.xr.runtime.math.Vector2 other);
    method public inline float getLength();
    method public inline float getLengthSquared();
    method public float getX();
    method public float getY();
    method public static androidx.xr.runtime.math.Vector2 lerp(androidx.xr.runtime.math.Vector2 start, androidx.xr.runtime.math.Vector2 end, float ratio);
    method public inline operator androidx.xr.runtime.math.Vector2 minus(androidx.xr.runtime.math.Vector2 other);
    method public operator androidx.xr.runtime.math.Vector2 plus(androidx.xr.runtime.math.Vector2 other);
    method public inline operator androidx.xr.runtime.math.Vector2 times(androidx.xr.runtime.math.Vector2 other);
    method public inline operator androidx.xr.runtime.math.Vector2 times(float c);
    method public androidx.xr.runtime.math.Vector2 toNormalized();
    method public inline operator androidx.xr.runtime.math.Vector2 unaryMinus();
    property public inline float length;
    property public inline float lengthSquared;
    property public float x;
    property public float y;
    field public static final androidx.xr.runtime.math.Vector2.Companion Companion;
    field public static final androidx.xr.runtime.math.Vector2 Down;
    field public static final androidx.xr.runtime.math.Vector2 Left;
    field public static final androidx.xr.runtime.math.Vector2 One;
    field public static final androidx.xr.runtime.math.Vector2 Right;
    field public static final androidx.xr.runtime.math.Vector2 Up;
    field public static final androidx.xr.runtime.math.Vector2 Zero;
  }

  public static final class Vector2.Companion {
    method public androidx.xr.runtime.math.Vector2 abs(androidx.xr.runtime.math.Vector2 vector);
    method public float angularDistance(androidx.xr.runtime.math.Vector2 vector1, androidx.xr.runtime.math.Vector2 vector2);
    method public float distance(androidx.xr.runtime.math.Vector2 vector1, androidx.xr.runtime.math.Vector2 vector2);
    method public androidx.xr.runtime.math.Vector2 lerp(androidx.xr.runtime.math.Vector2 start, androidx.xr.runtime.math.Vector2 end, float ratio);
    property public androidx.xr.runtime.math.Vector2 Down;
    property public androidx.xr.runtime.math.Vector2 Left;
    property public androidx.xr.runtime.math.Vector2 One;
    property public androidx.xr.runtime.math.Vector2 Right;
    property public androidx.xr.runtime.math.Vector2 Up;
    property public androidx.xr.runtime.math.Vector2 Zero;
  }

  public final class Vector3 {
    ctor public Vector3();
    ctor public Vector3(androidx.xr.runtime.math.Vector3 other);
    ctor public Vector3(optional float x);
    ctor public Vector3(optional float x, optional float y);
    ctor public Vector3(optional float x, optional float y, optional float z);
    method public static androidx.xr.runtime.math.Vector3 abs(androidx.xr.runtime.math.Vector3 vector);
    method public static float angleBetween(androidx.xr.runtime.math.Vector3 vector1, androidx.xr.runtime.math.Vector3 vector2);
    method public androidx.xr.runtime.math.Vector3 clamp(androidx.xr.runtime.math.Vector3 min, androidx.xr.runtime.math.Vector3 max);
    method public androidx.xr.runtime.math.Vector3 copy();
    method public androidx.xr.runtime.math.Vector3 copy(optional float x);
    method public androidx.xr.runtime.math.Vector3 copy(optional float x, optional float y);
    method public androidx.xr.runtime.math.Vector3 copy(optional float x, optional float y, optional float z);
    method public infix androidx.xr.runtime.math.Vector3 cross(androidx.xr.runtime.math.Vector3 other);
    method public static float distance(androidx.xr.runtime.math.Vector3 vector1, androidx.xr.runtime.math.Vector3 vector2);
    method public operator androidx.xr.runtime.math.Vector3 div(androidx.xr.runtime.math.Vector3 other);
    method public operator androidx.xr.runtime.math.Vector3 div(float c);
    method public infix float dot(androidx.xr.runtime.math.Vector3 other);
    method public static androidx.xr.runtime.math.Vector3 fromValue(float value);
    method public inline float getLength();
    method public inline float getLengthSquared();
    method public float getX();
    method public float getY();
    method public float getZ();
    method public static androidx.xr.runtime.math.Vector3 lerp(androidx.xr.runtime.math.Vector3 start, androidx.xr.runtime.math.Vector3 end, float ratio);
    method public static androidx.xr.runtime.math.Vector3 max(androidx.xr.runtime.math.Vector3 a, androidx.xr.runtime.math.Vector3 b);
    method public static androidx.xr.runtime.math.Vector3 min(androidx.xr.runtime.math.Vector3 a, androidx.xr.runtime.math.Vector3 b);
    method public operator androidx.xr.runtime.math.Vector3 minus(androidx.xr.runtime.math.Vector3 other);
    method public operator androidx.xr.runtime.math.Vector3 plus(androidx.xr.runtime.math.Vector3 other);
    method public static androidx.xr.runtime.math.Vector3 projectOnPlane(androidx.xr.runtime.math.Vector3 vector, androidx.xr.runtime.math.Vector3 planeNormal);
    method public operator androidx.xr.runtime.math.Vector3 times(androidx.xr.runtime.math.Vector3 other);
    method public operator androidx.xr.runtime.math.Vector3 times(float c);
    method public androidx.xr.runtime.math.Vector3 toNormalized();
    method public operator androidx.xr.runtime.math.Vector3 unaryMinus();
    property public inline float length;
    property public inline float lengthSquared;
    property public float x;
    property public float y;
    property public float z;
    field public static final androidx.xr.runtime.math.Vector3 Backward;
    field public static final androidx.xr.runtime.math.Vector3.Companion Companion;
    field public static final androidx.xr.runtime.math.Vector3 Down;
    field public static final androidx.xr.runtime.math.Vector3 Forward;
    field public static final androidx.xr.runtime.math.Vector3 Left;
    field public static final androidx.xr.runtime.math.Vector3 One;
    field public static final androidx.xr.runtime.math.Vector3 Right;
    field public static final androidx.xr.runtime.math.Vector3 Up;
    field public static final androidx.xr.runtime.math.Vector3 Zero;
  }

  public static final class Vector3.Companion {
    method public androidx.xr.runtime.math.Vector3 abs(androidx.xr.runtime.math.Vector3 vector);
    method public float angleBetween(androidx.xr.runtime.math.Vector3 vector1, androidx.xr.runtime.math.Vector3 vector2);
    method public float distance(androidx.xr.runtime.math.Vector3 vector1, androidx.xr.runtime.math.Vector3 vector2);
    method public androidx.xr.runtime.math.Vector3 fromValue(float value);
    method public androidx.xr.runtime.math.Vector3 lerp(androidx.xr.runtime.math.Vector3 start, androidx.xr.runtime.math.Vector3 end, float ratio);
    method public androidx.xr.runtime.math.Vector3 max(androidx.xr.runtime.math.Vector3 a, androidx.xr.runtime.math.Vector3 b);
    method public androidx.xr.runtime.math.Vector3 min(androidx.xr.runtime.math.Vector3 a, androidx.xr.runtime.math.Vector3 b);
    method public androidx.xr.runtime.math.Vector3 projectOnPlane(androidx.xr.runtime.math.Vector3 vector, androidx.xr.runtime.math.Vector3 planeNormal);
    property public androidx.xr.runtime.math.Vector3 Backward;
    property public androidx.xr.runtime.math.Vector3 Down;
    property public androidx.xr.runtime.math.Vector3 Forward;
    property public androidx.xr.runtime.math.Vector3 Left;
    property public androidx.xr.runtime.math.Vector3 One;
    property public androidx.xr.runtime.math.Vector3 Right;
    property public androidx.xr.runtime.math.Vector3 Up;
    property public androidx.xr.runtime.math.Vector3 Zero;
  }

  public final class Vector4 {
    ctor public Vector4();
    ctor public Vector4(androidx.xr.runtime.math.Vector4 other);
    ctor public Vector4(optional float x);
    ctor public Vector4(optional float x, optional float y);
    ctor public Vector4(optional float x, optional float y, optional float z);
    ctor public Vector4(optional float x, optional float y, optional float z, optional float w);
    method public static androidx.xr.runtime.math.Vector4 abs(androidx.xr.runtime.math.Vector4 vector);
    method public static float angleBetween(androidx.xr.runtime.math.Vector4 vector1, androidx.xr.runtime.math.Vector4 vector2);
    method public androidx.xr.runtime.math.Vector4 clamp(androidx.xr.runtime.math.Vector4 min, androidx.xr.runtime.math.Vector4 max);
    method public androidx.xr.runtime.math.Vector4 copy();
    method public androidx.xr.runtime.math.Vector4 copy(optional float x);
    method public androidx.xr.runtime.math.Vector4 copy(optional float x, optional float y);
    method public androidx.xr.runtime.math.Vector4 copy(optional float x, optional float y, optional float z);
    method public androidx.xr.runtime.math.Vector4 copy(optional float x, optional float y, optional float z, optional float w);
    method public static float distance(androidx.xr.runtime.math.Vector4 vector1, androidx.xr.runtime.math.Vector4 vector2);
    method public operator androidx.xr.runtime.math.Vector4 div(androidx.xr.runtime.math.Vector4 other);
    method public operator androidx.xr.runtime.math.Vector4 div(float c);
    method public infix float dot(androidx.xr.runtime.math.Vector4 other);
    method public static androidx.xr.runtime.math.Vector4 fromValue(float value);
    method public inline float getLength();
    method public inline float getLengthSquared();
    method public float getW();
    method public float getX();
    method public float getY();
    method public float getZ();
    method public static androidx.xr.runtime.math.Vector4 lerp(androidx.xr.runtime.math.Vector4 start, androidx.xr.runtime.math.Vector4 end, float ratio);
    method public static androidx.xr.runtime.math.Vector4 max(androidx.xr.runtime.math.Vector4 a, androidx.xr.runtime.math.Vector4 b);
    method public static androidx.xr.runtime.math.Vector4 min(androidx.xr.runtime.math.Vector4 a, androidx.xr.runtime.math.Vector4 b);
    method public operator androidx.xr.runtime.math.Vector4 minus(androidx.xr.runtime.math.Vector4 other);
    method public operator androidx.xr.runtime.math.Vector4 plus(androidx.xr.runtime.math.Vector4 other);
    method public operator androidx.xr.runtime.math.Vector4 times(androidx.xr.runtime.math.Vector4 other);
    method public operator androidx.xr.runtime.math.Vector4 times(float c);
    method public androidx.xr.runtime.math.Vector4 toNormalized();
    method public operator androidx.xr.runtime.math.Vector4 unaryMinus();
    property public inline float length;
    property public inline float lengthSquared;
    property public float w;
    property public float x;
    property public float y;
    property public float z;
    field public static final androidx.xr.runtime.math.Vector4.Companion Companion;
    field public static final androidx.xr.runtime.math.Vector4 One;
    field public static final androidx.xr.runtime.math.Vector4 Zero;
  }

  public static final class Vector4.Companion {
    method public androidx.xr.runtime.math.Vector4 abs(androidx.xr.runtime.math.Vector4 vector);
    method public float angleBetween(androidx.xr.runtime.math.Vector4 vector1, androidx.xr.runtime.math.Vector4 vector2);
    method public float distance(androidx.xr.runtime.math.Vector4 vector1, androidx.xr.runtime.math.Vector4 vector2);
    method public androidx.xr.runtime.math.Vector4 fromValue(float value);
    method public androidx.xr.runtime.math.Vector4 lerp(androidx.xr.runtime.math.Vector4 start, androidx.xr.runtime.math.Vector4 end, float ratio);
    method public androidx.xr.runtime.math.Vector4 max(androidx.xr.runtime.math.Vector4 a, androidx.xr.runtime.math.Vector4 b);
    method public androidx.xr.runtime.math.Vector4 min(androidx.xr.runtime.math.Vector4 a, androidx.xr.runtime.math.Vector4 b);
    property public androidx.xr.runtime.math.Vector4 One;
    property public androidx.xr.runtime.math.Vector4 Zero;
  }

}

