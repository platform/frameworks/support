# Jetpack XR Assets
This library provides a convenient entrypoint for assets to be used across the
Jetpack XR libraries for testing/sample purposes. Additional assets may not be
added in other XR libraries to avoid file duplication.

The files are actually located in the prebuilts/androidx/xr/assets directory
and loaded for convenience in this project. All files in that directory were
created for testing purposes and are released under the terms of the Apache 2.0
license.
