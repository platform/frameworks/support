/*
 * Copyright 2024 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package androidx.compose.foundation.text.contextmenu.modifier

import androidx.compose.foundation.internal.checkPreconditionNotNull
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.node.ModifierNodeElement
import androidx.compose.ui.test.junit4.createComposeRule
import com.google.common.truth.Truth.assertThat
import org.junit.Rule
import org.junit.Test

class TextContextMenuModifierTraversalTest {
    @get:Rule val rule = createComposeRule()

    @Test
    fun whenNoModifiers_noInvocations() {
        val invocations = mutableListOf<String>()
        setContentAndInvokeContextMenuData { dataReadingContent -> dataReadingContent() }

        assertThat(invocations).isEmpty()
    }

    @Test
    fun whenOneModifier_nodeTraversed() {
        val invocations = mutableListOf<String>()
        setContentAndInvokeContextMenuData { dataReadingContent ->
            Box(Modifier.addTextContextMenuComponents { invocations += "node" }) {
                dataReadingContent()
            }
        }

        assertThat(invocations).containsExactly("node").inOrder()
    }

    @Test
    fun whenChainedModifier_nodesTraversedBottomToTop() {
        val invocations = mutableListOf<String>()
        setContentAndInvokeContextMenuData { dataReadingContent ->
            Box(
                Modifier.addTextContextMenuComponents { invocations += "outer" }
                    .addTextContextMenuComponents { invocations += "inner" }
            ) {
                dataReadingContent()
            }
        }

        assertThat(invocations).containsExactly("inner", "outer").inOrder()
    }

    @Test
    fun whenNestedModifier_nodesTraversedBottomToTop() {
        val invocations = mutableListOf<String>()
        setContentAndInvokeContextMenuData { dataReadingContent ->
            Box(Modifier.addTextContextMenuComponents { invocations += "outer" }) {
                Box(Modifier.addTextContextMenuComponents { invocations += "inner" }) {
                    dataReadingContent()
                }
            }
        }

        assertThat(invocations).containsExactly("inner", "outer").inOrder()
    }

    @Test
    fun whenMultipleNodes_doesNotTraverseNodesNotInAncestry() {
        val invocations = mutableListOf<String>()
        setContentAndInvokeContextMenuData { dataReadingContent ->
            Column(Modifier.addTextContextMenuComponents { invocations += "outer" }) {
                Box(Modifier.addTextContextMenuComponents { invocations += "inner1" }) {
                    dataReadingContent()
                }
                Box(Modifier.addTextContextMenuComponents { invocations += "inner2" })
            }
        }

        assertThat(invocations).containsExactly("inner1", "outer").inOrder()
    }

    @Test
    fun whenDeeperChildNodes_doesNotTraverseNodesNotInAncestry() {
        val invocations = mutableListOf<String>()
        setContentAndInvokeContextMenuData { dataReadingContent ->
            Column(Modifier.addTextContextMenuComponents { invocations += "outer" }) {
                dataReadingContent()
                Box(Modifier.addTextContextMenuComponents { invocations += "inner1" })
                Box(Modifier.addTextContextMenuComponents { invocations += "inner2" })
            }
        }

        assertThat(invocations).containsExactly("outer").inOrder()
    }

    @Test
    fun whenFilters_filtersAppliedAfterAllBuilders() {
        val invocations = mutableListOf<String>()
        setContentAndInvokeContextMenuData { dataReadingContent ->
            Box(
                Modifier.fakeFilterTextContextMenuComponents { invocations += "outer filter" }
                    .addTextContextMenuComponents { invocations += "outer builder" }
                    .addTextContextMenuComponents { invocations += "inner builder" }
                    .fakeFilterTextContextMenuComponents { invocations += "inner filter" }
                    .addTextContextMenuComponents {
                        // Add an item otherwise the filters won't have anything to run on.
                        item("key", "label") { /* No action */ }
                    }
            ) {
                dataReadingContent()
            }
        }

        // We expect inner then outer builders, and then the filters in any order.
        assertThat(invocations)
            .containsExactly("inner builder", "outer builder", "inner filter", "outer filter")
        assertThat(invocations[0]).isEqualTo("inner builder")
        assertThat(invocations[1]).isEqualTo("outer builder")
        assertThat(invocations[2]).isAnyOf("inner filter", "outer filter")
        assertThat(invocations[3]).isAnyOf("inner filter", "outer filter")
    }

    private fun setContentAndInvokeContextMenuData(
        outerContent: @Composable (dataReadingContent: @Composable () -> Unit) -> Unit,
    ) {
        val reader = TestTextContextMenuDataInvoker()
        rule.setContent { outerContent { Box(TestTextContextMenuDataReaderElement(reader)) } }
        reader.invokeTraversal()
    }
}

/** Same as [filterTextContextMenuComponents] but puts defaults into the lambda args/returns. */
private fun Modifier.fakeFilterTextContextMenuComponents(block: () -> Unit): Modifier =
    filterTextContextMenuComponents {
        block()
        true
    }

private class TestTextContextMenuDataInvoker {
    var node: TestTextContextMenuDataReaderNode? = null

    fun invokeTraversal() {
        checkPreconditionNotNull(node).collectTextContextMenuData()
    }
}

private data class TestTextContextMenuDataReaderElement(
    val invoker: TestTextContextMenuDataInvoker,
) : ModifierNodeElement<TestTextContextMenuDataReaderNode>() {
    override fun create(): TestTextContextMenuDataReaderNode =
        TestTextContextMenuDataReaderNode(invoker)

    override fun update(node: TestTextContextMenuDataReaderNode) {
        node.update(invoker)
    }
}

private class TestTextContextMenuDataReaderNode(
    invoker: TestTextContextMenuDataInvoker,
) : Modifier.Node() {
    var invoker: TestTextContextMenuDataInvoker = invoker
        private set

    override fun onAttach() {
        super.onAttach()
        invoker.node = this
    }

    override fun onDetach() {
        invoker.node = null
        super.onDetach()
    }

    fun update(reader: TestTextContextMenuDataInvoker) {
        this.invoker = reader
    }
}
