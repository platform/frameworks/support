/*
 * Copyright 2022 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
// VERSION: 14_1_0
// GENERATED CODE - DO NOT MODIFY BY HAND

package androidx.compose.material3.tokens

internal object OutlinedIconButtonTokens {
    val DisabledColor = ColorSchemeKeyTokens.OnSurface
    val DisabledOpacity = 0.38f
    val DisabledOutlineColor = ColorSchemeKeyTokens.OutlineVariant
    val FocusedColor = ColorSchemeKeyTokens.OnSurfaceVariant
    val HoveredColor = ColorSchemeKeyTokens.OnSurfaceVariant
    val Color = ColorSchemeKeyTokens.OnSurfaceVariant
    val OutlineColor = ColorSchemeKeyTokens.OutlineVariant
    val PressedColor = ColorSchemeKeyTokens.OnSurfaceVariant
    val SelectedContainerColor = ColorSchemeKeyTokens.InverseSurface
    val SelectedDisabledContainerColor = ColorSchemeKeyTokens.OnSurface
    val SelectedDisabledContainerOpacity = 0.1f
    val SelectedFocusedColor = ColorSchemeKeyTokens.InverseOnSurface
    val SelectedHoveredColor = ColorSchemeKeyTokens.InverseOnSurface
    val SelectedColor = ColorSchemeKeyTokens.InverseOnSurface
    val SelectedPressedColor = ColorSchemeKeyTokens.InverseOnSurface
    val UnselectedDisabledOutlineColor = ColorSchemeKeyTokens.OutlineVariant
    val UnselectedFocusedColor = ColorSchemeKeyTokens.OnSurfaceVariant
    val UnselectedHoveredColor = ColorSchemeKeyTokens.OnSurfaceVariant
    val UnselectedColor = ColorSchemeKeyTokens.OnSurfaceVariant
    val UnselectedOutlineColor = ColorSchemeKeyTokens.OutlineVariant
    val UnselectedPressedColor = ColorSchemeKeyTokens.OnSurfaceVariant
}
