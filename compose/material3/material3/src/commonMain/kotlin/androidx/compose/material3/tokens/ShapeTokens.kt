/*
 * Copyright 2021 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
// VERSION: 14_1_0
// GENERATED CODE - DO NOT MODIFY BY HAND

package androidx.compose.material3.tokens

import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.unit.dp

internal object ShapeTokens {
    val CornerExtraExtraLarge = RoundedCornerShape(48.0.dp)
    val CornerExtraLarge = RoundedCornerShape(28.0.dp)
    val CornerExtraLargeIncreased = RoundedCornerShape(32.0.dp)
    val CornerExtraLargeTop =
        RoundedCornerShape(
            topStart = 28.0.dp,
            topEnd = 28.0.dp,
            bottomEnd = 0.0.dp,
            bottomStart = 0.0.dp,
        )
    val CornerExtraSmall = RoundedCornerShape(4.0.dp)
    val CornerExtraSmallTop =
        RoundedCornerShape(
            topStart = 4.0.dp,
            topEnd = 4.0.dp,
            bottomEnd = 0.0.dp,
            bottomStart = 0.0.dp
        )
    val CornerFull = CircleShape
    val CornerLarge = RoundedCornerShape(16.0.dp)
    val CornerLargeEnd =
        RoundedCornerShape(
            topStart = 0.0.dp,
            topEnd = 16.0.dp,
            bottomEnd = 16.0.dp,
            bottomStart = 0.0.dp,
        )
    val CornerLargeIncreased = RoundedCornerShape(20.0.dp)
    val CornerLargeStart =
        RoundedCornerShape(
            topStart = 16.0.dp,
            topEnd = 0.0.dp,
            bottomEnd = 0.0.dp,
            bottomStart = 16.0.dp,
        )
    val CornerLargeTop =
        RoundedCornerShape(
            topStart = 16.0.dp,
            topEnd = 16.0.dp,
            bottomEnd = 0.0.dp,
            bottomStart = 0.0.dp,
        )
    val CornerMedium = RoundedCornerShape(12.0.dp)
    val CornerNone = RectangleShape
    val CornerSmall = RoundedCornerShape(8.0.dp)
    val CornerValueExtraExtraLarge = CornerSize(48.0.dp)
    val CornerValueExtraLarge = CornerSize(28.0.dp)
    val CornerValueExtraLargeIncreased = CornerSize(32.0.dp)
    val CornerValueExtraSmall = CornerSize(4.0.dp)
    val CornerValueLarge = CornerSize(16.0.dp)
    val CornerValueLargeIncreased = CornerSize(20.0.dp)
    val CornerValueMedium = CornerSize(12.0.dp)
    val CornerValueNone = CornerSize(0.0.dp)
    val CornerValueSmall = CornerSize(8.0.dp)
}
