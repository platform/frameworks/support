# Module root

androidx.credentials.providerevents providerevents

# Package androidx.credentials.providerevents

Allows credential providers to extend services that propagate update events
